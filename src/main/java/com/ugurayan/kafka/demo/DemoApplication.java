package com.ugurayan.kafka.demo;

import com.ugurayan.kafka.demo.kafka.KafkaProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication implements CommandLineRunner {

    @Autowired KafkaProducer sender;

    public static void main(String[] args)  {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        KafkaProducer producer = new KafkaProducer();
        for ( int i =1 ; i < 10000; i ++){
            sender.send("test123", "Message : " + i);
        }

    }
}
